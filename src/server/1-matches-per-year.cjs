const csvToJson = require("csvtojson");
const fs = require("fs");
const path = require("path");
const matchesPath = path.resolve(__dirname, "../data/matches.csv");

function matchesPerYear() {
  const data = csvToJson()
    .fromFile(matchesPath)
    .then((data) => {
      const matchPerYear = {};
      data.map((item) => {
        if (matchPerYear[item.season] == undefined) {
          matchPerYear[item.season] = 1;
        } else {
          matchPerYear[item.season] = matchPerYear[item.season] + 1;
        }
      });
      return matchPerYear;
    });
  return data;
}

module.exports = matchesPerYear;

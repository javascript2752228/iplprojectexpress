const csvToJson = require("csvtojson");
const path = require("path");

const matchesPath = path.resolve(__dirname, "../data/matches.csv");

function matchesWonPerTeamPerYear() {
  let answer2 = csvToJson()
    .fromFile(matchesPath)
    .then((data) => {
      let ansObject = data.reduce((acc, item) => {
        if (acc[item.winner] === undefined) {
          acc[item.winner] = {};
        }
        if (acc[item.winner][item.season] === undefined) {
          acc[item.winner][item.season] = 0;
        }
        acc[item.winner][item.season] += 1;
        return acc;
      }, {});
      return ansObject;
    })
    .catch((error) => {
      console.error(error);
    });
  return answer2;
}

module.exports = matchesWonPerTeamPerYear;

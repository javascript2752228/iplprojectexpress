const express = require("express");
const csvToJson = require("csvtojson");
const app = express();

const matchesPerYear = require("./src/server/1-matches-per-year.cjs");
const matchesWonPerTeamPerYear = require("./src/server/2-matches-won-per-year.cjs");
const extraRunsConcededPerTeam = require("./src/server/3-extra-run-conceded-in-2016.cjs");
const economicalBowlerIn2015 = require("./src/server/4-top-10-economical-bowlers-2015.cjs");
const tossWonMatchWon = require("./src/server/5-toss-won-match-won.cjs");
const highestPlayerOfTheMatchPerSeason = require("./src/server/6-player-of-match.cjs");
const strikeRatePerYearPerPerson = require("./src/server/7-strike-rate-of-batsman-for-each-season.cjs");
const dissmissalCount = require("./src/server/8-highest-number-of-times-player-dismissed.cjs");
const bestEconomyPlayerInSuperOver = require("./src/server/9-bowler-with-best-economy-super-over.cjs");

app.get("/matches-per-year", (request, response) => {
  matchesPerYear()
    .then((data) => {
      console.log(data);
      response.json(data);
      return;
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/matches-won-per-team-per-year", (request, response) => {
  matchesWonPerTeamPerYear()
    .then((data) => {
      response.json(data);
      return;
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/extra-runs-conceded-per-team-2016", (request, response) => {
  extraRunsConcededPerTeam()
    .then((data) => {
      response.json(data);
      return;
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/top-10-economical-bowler", (request, response) => {
  economicalBowlerIn2015()
    .then((data) => {
      response.json(data);
      return;
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/best-economy-in-super-over", (request, response) => {
  bestEconomyPlayerInSuperOver()
    .then((data) => {
      response.json(data);
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/toss-and-match-both-won", (request, response) => {
  tossWonMatchWon()
    .then((data) => {
      response.json(data);
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/highest-player-of-matches-award", (request, response) => {
  highestPlayerOfTheMatchPerSeason()
    .then((data) => {
      response.json(data);
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/strike-rate-of-batsman-for-each-season", (request, response) => {
  strikeRatePerYearPerPerson()
    .then((data) => {
      response.json(data);
    })
    .catch((error) => {
      console.error(error);
    });
});

app.get("/highest-number-of-time-player-dismissed", (request, response) => {
  dissmissalCount()
    .then((data) => {
      response.json(data);
    })
    .catch((error) => {
      console.error(error);
    });
});

const port = process.env.PORT || 5000;

app.listen(port, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.log("Server is up and running successfully on port:", port);
  }
});
